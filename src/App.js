import React,{useState} from 'react';
// import { v4 as uuidv4 } from 'uuid';
// import logo from './logo.svg';
import './App.css';
import Leftbar from './components/leftbar';
import Section from './components/Section';

function App() {
  const [viewIn, setViewIn] = useState('Portfolio')

  const onClick = e => {
    setViewIn(e.currentTarget.getAttribute('render'))
  }
  return (
    <div className="App">
      <div className="container-fluid p-0">
      <div className="row ">
      <Leftbar onClick={onClick} viewIn={viewIn}/>
      <Section viewIn={viewIn}/>
      </div>
      </div>
		</div>
    
	
  );
}

export default App;
