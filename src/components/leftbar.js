import React from 'react'


var Leftbar = (props) =>

{
    return (
        
      <div className="col-md-2 left-content pd-r0">
      <header id="header">
		  <div className="main-header">
        <div className="img-profile">
          <img src={process.env.PUBLIC_URL +"/img/profile.jpg"} alt=""/>
          <div className="name-profile t-center">
            <h5 className="uppercase">Andrew Ramy</h5>
          </div>    
        </div>
			  <nav id="main-nav" className="main-nav clearfix t-center uppercase tabbed">
						<ul className="clearfix">
            <li ><a onClick={props.onClick} render="Portfolio" className={props.viewIn === 'Portfolio' ? 'active' : ''}><i className="icon-camera"></i>Portfolio<span>my work</span></a></li>
							<li ><a onClick={props.onClick} render="About" className={props.viewIn === 'About' ? 'active' : ''}>
                                <i className="icon-user"></i>About me<span>who am i</span></a>
                            </li>
							<li ><a onClick={props.onClick} render="Resume" className={props.viewIn === 'Resume' ? 'active' : ''}><i className="icon-shopping-bag"></i>Resume<span>curriculum vita</span></a></li>
							
						</ul>

				</nav>
					
							
			</div>

      </header>	
      </div>
      
        );
}

export default Leftbar;