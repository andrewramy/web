import React from 'react'


var About = (props) =>

{
    return (

<section className="bg-white t-center">
	<div className="about">
        <div className="content">
            <div className="block-content mb-100">
                <div className="row">
                    <div className="col-md-12  ">
                        <div className="main-title profile">
	                        <h1 className="mb-20">Andrew Ramy</h1>
	                        <h3>Web Developer</h3>
	                    </div>
					</div>
				</div>
            </div>
            <div className="block-content mb-100">
                <div className="row">
                    <div className="col-md-12">
                        <div className="sub-title mb-40">
				            <h2 className="uppercase">About Me</h2>
				        </div>
                    </div>
                </div>
                <p className="lead-intro">I am a web developer. I started working as a web developer since 2020. </p>
				</div>
				<div className="block-content mb-100 pb-30">
                    <div className="row">
                        <div className="col-md-12  ">
                            <div className="sub-title mb-40">
					            <h2 className="uppercase">Personal Info</h2>
					        </div>
                        </div>
					</div>
			
					<div className="row">
						<div className="col-md-12 col-md-offset-3  col-sm-12 col-sm-offset-3 ">
							<div className="listing mt-40">
								<div className="listing-inner">
									<div className="listing-event">
										<ul className="data left">
											<li className="emph-1">Name</li>
											<li className="emph-1">Birthday</li>
											<li className="emph-1">Nationality</li>
											<li className="emph-1">Marital Status</li>
											<li className="emph-1">Freelance</li>	
										</ul>
										<ul className="data right">
											<li>Andrew Ramy Edwar</li>
											<li>1994.04.19 <span className="emph-1">(year/month/day)</span></li>
											<li>EGYPTIAN</li>
											<li>Single</li>
											<li>Available</li>
										</ul>	
									</div>
								</div>
							</div>											
						</div>
					</div>
                </div>
				<div className="block-content mb-100  pb-30">
                    <div className="row">
                        <div className="col-md-12  ">
                            <div className="sub-title mb-40">
				                <h2 className="uppercase">Contact Info</h2>
				            </div>
                        </div>
					</div>
					<div className="row">
						<div className="col-md-12 col-md-offset-3  col-sm-12 col-sm-offset-3 ">
							<div className="listing mt-40">	
							    <div className="listing-inner">
									<div className="listing-event">
										<ul className="data left">
											<li><span className="emph-1">E-Mail</span> :<br />
											</li>
                                            <li><span className="emph-1">Phone</span> :<br />
											</li>
										</ul>

										<ul className="data right">
											<li><span className="emph-1"></span> : <a href="mailto: andrew_ramy@outlook.com">andrew_ramy@outlook.com</a></li>
											<li><span className="emph-1"></span> :  <a href="tel:01274546014">+ 20 1274546014</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
</section>
        );
}

export default About;