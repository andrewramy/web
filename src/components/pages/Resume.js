import React from 'react'


var Resume = (props) =>

{
    return (
<section  className="bg-white t-center">
	<div className="resume">
        <div className="content">
             <div className="block-content mb-100">
                <div className="row">
                    <div className="col-md-12  ">       
                        <div className="main-title">
                            <h1 className="mb-20">Resume</h1>
                        </div> 
                    </div>
                </div>
            </div>
           
            <div className="block-content mb-100  pb-10">
                <div className="row">
                    <div className="col-md-12  ">
                        <div className="sub-title mb-40">
                            <h2 className="uppercase">Education</h2>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12  ">
                        <div className="timeline">
                            <div className="timeline-inner">
                                <div className="name">
                                    <span className="date">2017</span>
                                    <h4>Bachelor of Architecture  </h4>
                                </div>
                                <div className="detail">
                                    <p>faculty of fine arts architecture department - Helwan university 2017. </p>
                                </div>
                            </div>
                            <div className="timeline-inner ">
                                <div className="name switched">
                                    <span className="date">2019</span>
                                    <h4>Master Web Design</h4> 
                                </div>
                                <div className="detail">
                                    <p>ITShare Training Center 2019.</p>
                                </div>
                             </div>
                            <div className="timeline-inner">
                                <div className="name switched">
                                    <span className="date">2020</span>
                                    <h4>Master of Back-End</h4>
                                </div>
                                <div className="detail">
                                    <p>ITShare Training Center 2020. </p>
                                </div>
                            </div>
                            <div className="timeline-inner">
                                <div className="name switched">
                                    <span className="date">2021</span>
                                    <h4>Web Design</h4>
                                </div>
                                <div className="detail">
                                    <p>NTI 2021. </p>
                                </div>
                            </div>
                            <div className="timeline-inner">
                                <div className="name">
                                    <span className="date">2021</span>
                                    <h4>Web Development using PHP , MySQL and Laravel</h4>
                                </div>
                                <div className="detail">
                                    <p>NTI 2021. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
            <div className="block-content mb-100  pb-30">
                <div className="row">      
                    <div className="col-md-12  ">          
                        <div className="sub-title mb-40">
                            <h2 className="uppercase">skills</h2>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12  ">
                        <div className="listing-large mt-40">
                            <div className="listing-large-inner">
                                <div className="listing-event">
                                    <ul className="data left clearfix">
                                    <li>
                                            <h5>Photoshop</h5>
                                        </li>
                                        <li>
                                             <h5>Illustrator</h5>
                                        </li>
                                        <li>
                                             <h5>Adobe XD</h5>
                                        </li>
                                        <li>
                                             <h5>Html</h5>
                                         </li>
                                                    
                                         <li>
                                            <h5>Css</h5>
                                        </li>
                                        <li>
                                            <h5>Scss</h5>
                                        </li>
                                        <li>
                                            <h5>Bootstrap</h5>
                                        </li>         
                                         <li>
                                            <h5>JavaScript</h5>
                                        </li>
                                         <li>
                                            <h5>Jquery</h5>
                                        </li>
                                        <li>
                                            <h5>React.js</h5>
                                        </li>
                                        <li>
                                            <h5>Vue.js</h5>
                                        </li>        
                                        <li>
                                            <h5>Alpine.js</h5>
                                        </li> 
                                        
                                     </ul>
                                                                                        
                                            <ul className="data right clearfix">
                                                
                                            
                                              
                                        <li>
                                            <h5>Php</h5>
                                         </li>    
                                         <li>
                                            <h5>Laravel</h5>
                                         </li>
                                         <li>
                                            <h5>Livewire</h5>
                                         </li>
                                                    
                                        <li>
                                           <h5>Node.js</h5>   
                                        </li>    
                                        <li>
                                           <h5>socet.io</h5>   
                                        </li>    
                                        <li>
                                           <h5>MySQL</h5>   
                                        </li>    
                                        <li>
                                           <h5>MongoDB</h5>   
                                        </li>    
                                                
                    
                                                </ul>																	
                                                    
                                                
                                        </div>
                                        </div>
                                    
                                    </div>
                    
                                 
                    
                                        </div>
                                        </div>
                                     
                                    
                                                
                                            </div>
                                           
                                            
                                                                            
                                            	
                                       
                    
                                        </div>
                                       
                    
                                      
                                                
                                                
                                                            
                    
                                          
                                        
                                        </div>
                    
                                       
                                      
                                        
                                    
                                   
                            
                                </section>

        );
}

export default Resume;