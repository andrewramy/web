import React from 'react'


var Section = (props) => {
    var work = [
        {
            name: 'Taskick',
            img: '/img/Taskick.png',
            url: 'https://taskick.net/',
            desc:"The website features mange tasks in website and create api for desktop app."
        },
        {
            name: 'SIDEUP SAUDI',
            img: '/img/SIDEUP-sa.png',
            url: 'https://sa.sideup.co/',
            desc:"The website features multi tenant , integrations with couriers ,integrations with payments getway , add new features to dashboard.  "
        },
        {
            name: 'SIDEUP EGYPT',
            img: '/img/SIDEUP-eg.png',
            url: 'https://eg.sideup.co/',
            desc:"The website features multi tenant , integrations with couriers ,integrations with payments getway , add new features to dashboard.  "
        },
        {
            name: 'ALATRAF SAUDI',
            img: '/img/Alatraf.png',
            url: 'https://alatraf.sa',
            desc:"The website features multi tenant  ,integrations with payments getway , online shopping website , multi vendors , real time events with socket.   "
        },
        {
            name: 'Ghayatcom',
            img: '/img/Ghayatcom.png',
            url: 'https://ghayatcom.sa/',
            desc:"The website features healthy reservation system.  "
        },
        {
            name: 'Dayemgroups',
            img: '/img/Dayem.png',
            url: 'https://www.dayemgroups.com/',
            desc:"The website features ecom web site and mobile app."
        },
        {
            name: 'Chat App',
            img: '/img/chat.png',
            url: 'https://andrew-ramy-chat-app.herokuapp.com/',
            desc:"The website features node js video call and real time chat by socket io.   "
        },
        


    ];
    return (

        <section className="bg-white t-center">
            <div className="portfolio">
                <div className="content">
                    <div className="block-content mb-100">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="main-title ">
                                    <h1 className="mb-20">Portfolio</h1>
                                    <h4 className="uppercase">My Projects</h4>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="main">
                        <ul className="cards">



                            {work.map((item, index) => {
                                return (
                                    <li className="cards_item" key={index}>
                                        <div className="mycard" tabindex="0">
                                            <div className="card_image"><img src={process.env.PUBLIC_URL + item.img} alt="" /></div>
                                            <div className="card_content">
                                                <h2 className="card_title">{item.name}</h2>
                                                <div className="card_text">
                                                    <p><strong>{item.name}</strong></p>
                                                    <p>{ item.desc }</p>
                                                    <a className="upcharge" href={item.url} target='_blank'>GO</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                )
                            })}

                        </ul>
                    </div>














                </div>
            </div>
        </section>
    );
}

export default Section;