import React from 'react';
import About from './pages/About';
import Profile from './pages/Profile';
import Resume from './pages/Resume';
var Section = (props) =>

{
    return (
        <div className="col-md-10 right-content pd-r0 pd-l0">

        {props.viewIn === 'Portfolio' ? <Profile /> : ''}
        {props.viewIn === 'About' ? <About /> : ''}
        {props.viewIn === 'Resume' ? <Resume /> : ''}
        </div>
        );
}

export default Section;